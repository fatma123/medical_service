<?php

session_start();

define("BURL",'http://127.0.0.1:8000/'); //Base url for site
define("BURLA",'http://127.0.0.1:8000/admin/'); //Base url for admin panel
define("ASSETS",'http://127.0.0.1:8000/assets/'); // Base url for  assets files

define("BL",__DIR__.'/'); // Base link for site
define("BLA",__DIR__.'/admin/'); // Base link for Admin

//Connect to database
require_once(BL.'functions/db.php');